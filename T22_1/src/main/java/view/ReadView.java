package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.MainController;
import model.dto.Cliente;

import javax.swing.SpringLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JTextArea;

public class ReadView extends JFrame {

	private JPanel contentPane;
	private JTextField inputId;
	public static JTextArea txtAreaResult;

	public ReadView() {
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);
		
		JLabel lblId = new JLabel("Id");
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblId, 10, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblId, 10, SpringLayout.WEST, contentPane);
		contentPane.add(lblId);
		
		inputId = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, inputId, 6, SpringLayout.SOUTH, lblId);
		sl_contentPane.putConstraint(SpringLayout.WEST, inputId, 0, SpringLayout.WEST, lblId);
		contentPane.add(inputId);
		inputId.setColumns(10);
		
		JButton btnRead = new JButton("Read");
		sl_contentPane.putConstraint(SpringLayout.SOUTH, btnRead, 0, SpringLayout.SOUTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, btnRead, -10, SpringLayout.EAST, contentPane);
		contentPane.add(btnRead);
		btnRead.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Creamos un objeto cliente, le setteamos los datos y se lo pasamos al metodo correspondiente del controller
				Cliente newCliente = new Cliente();
				newCliente.setId(Integer.parseInt(inputId.getText()));
				
				MainController.readCliente(newCliente);
			}
		});
		
		txtAreaResult = new JTextArea();
		sl_contentPane.putConstraint(SpringLayout.NORTH, txtAreaResult, 37, SpringLayout.SOUTH, inputId);
		sl_contentPane.putConstraint(SpringLayout.WEST, txtAreaResult, 15, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, txtAreaResult, -11, SpringLayout.NORTH, btnRead);
		sl_contentPane.putConstraint(SpringLayout.EAST, txtAreaResult, -10, SpringLayout.EAST, contentPane);
		contentPane.add(txtAreaResult);
		
		JLabel lblResult = new JLabel("Result:");
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblResult, 11, SpringLayout.SOUTH, inputId);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblResult, 13, SpringLayout.WEST, contentPane);
		contentPane.add(lblResult);
	}
}
