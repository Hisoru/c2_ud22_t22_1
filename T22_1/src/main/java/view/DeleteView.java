package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.MainController;
import model.dto.Cliente;

import javax.swing.SpringLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

public class DeleteView extends JFrame {

	private JPanel contentPane;
	private JTextField inputId;

	public DeleteView() {
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);
		
		JLabel lblId = new JLabel("Id");
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblId, 10, SpringLayout.NORTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblId, 10, SpringLayout.WEST, contentPane);
		contentPane.add(lblId);
		
		inputId = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, inputId, 9, SpringLayout.SOUTH, lblId);
		sl_contentPane.putConstraint(SpringLayout.WEST, inputId, 0, SpringLayout.WEST, lblId);
		contentPane.add(inputId);
		inputId.setColumns(10);
		
		JButton btnDelete = new JButton("Delete");
		sl_contentPane.putConstraint(SpringLayout.SOUTH, btnDelete, -10, SpringLayout.SOUTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, btnDelete, -10, SpringLayout.EAST, contentPane);
		contentPane.add(btnDelete);
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Creamos un objeto cliente, le setteamos los datos y se lo pasamos al metodo correspondiente del controller
				Cliente newCliente = new Cliente();
				newCliente.setId(Integer.parseInt(inputId.getText()));
				
				MainController.deleteCliente(newCliente);
			}
		});
	}

}
