package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.MainController;
import model.dto.Cliente;

import javax.swing.SpringLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

public class InsertView extends JFrame {

	private JPanel contentPane;
	private JTextField inputNombre;
	private JLabel lblNombre;
	private JTextField inputApellido;
	private JLabel lblApellido;
	private JLabel lblDireccion;
	private JTextField inputDireccion;
	private JLabel lblDni;
	private JTextField inputDni;
	private JTextField inputFecha;
	private JLabel lblFecha;

	public InsertView() {
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		SpringLayout sl_contentPane = new SpringLayout();
		contentPane.setLayout(sl_contentPane);
		
		inputNombre = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.WEST, inputNombre, 10, SpringLayout.WEST, contentPane);
		inputNombre.setColumns(10);
		contentPane.add(inputNombre);
		
		lblNombre = new JLabel("Nombre");
		sl_contentPane.putConstraint(SpringLayout.NORTH, inputNombre, 6, SpringLayout.SOUTH, lblNombre);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblNombre, 10, SpringLayout.WEST, contentPane);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, lblNombre, -222, SpringLayout.SOUTH, contentPane);
		contentPane.add(lblNombre);
		
		inputApellido = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.WEST, inputApellido, 0, SpringLayout.WEST, inputNombre);
		inputApellido.setColumns(10);
		contentPane.add(inputApellido);
		
		lblApellido = new JLabel("Apellido");
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblApellido, 14, SpringLayout.SOUTH, inputNombre);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblApellido, 0, SpringLayout.WEST, inputNombre);
		sl_contentPane.putConstraint(SpringLayout.NORTH, inputApellido, 6, SpringLayout.SOUTH, lblApellido);
		contentPane.add(lblApellido);
		
		lblDireccion = new JLabel("Direccion");
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblDireccion, 17, SpringLayout.SOUTH, inputApellido);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblDireccion, 0, SpringLayout.WEST, inputNombre);
		contentPane.add(lblDireccion);
		
		inputDireccion = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.NORTH, inputDireccion, 6, SpringLayout.SOUTH, lblDireccion);
		sl_contentPane.putConstraint(SpringLayout.WEST, inputDireccion, 0, SpringLayout.WEST, inputNombre);
		inputDireccion.setColumns(10);
		contentPane.add(inputDireccion);
		
		lblDni = new JLabel("Dni");
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblDni, 16, SpringLayout.SOUTH, inputDireccion);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblDni, 10, SpringLayout.WEST, contentPane);
		contentPane.add(lblDni);
		
		inputDni = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.SOUTH, lblDni, -6, SpringLayout.NORTH, inputDni);
		sl_contentPane.putConstraint(SpringLayout.NORTH, inputDni, 36, SpringLayout.SOUTH, inputDireccion);
		sl_contentPane.putConstraint(SpringLayout.EAST, inputDni, 0, SpringLayout.EAST, inputNombre);
		inputDni.setColumns(10);
		contentPane.add(inputDni);
		
		JButton btnInsert = new JButton("Insert");
		sl_contentPane.putConstraint(SpringLayout.SOUTH, btnInsert, -10, SpringLayout.SOUTH, contentPane);
		sl_contentPane.putConstraint(SpringLayout.EAST, btnInsert, -10, SpringLayout.EAST, contentPane);
		contentPane.add(btnInsert);
		
		inputFecha = new JTextField();
		sl_contentPane.putConstraint(SpringLayout.WEST, inputFecha, 37, SpringLayout.EAST, inputDni);
		sl_contentPane.putConstraint(SpringLayout.SOUTH, inputFecha, 0, SpringLayout.SOUTH, inputDni);
		sl_contentPane.putConstraint(SpringLayout.EAST, inputFecha, 165, SpringLayout.EAST, inputDni);
		inputFecha.setColumns(10);
		contentPane.add(inputFecha);
		
		lblFecha = new JLabel("Fecha");
		sl_contentPane.putConstraint(SpringLayout.NORTH, lblFecha, 0, SpringLayout.NORTH, lblDni);
		sl_contentPane.putConstraint(SpringLayout.WEST, lblFecha, 108, SpringLayout.EAST, lblDni);
		contentPane.add(lblFecha);
		btnInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Creamos un objeto cliente, le setteamos los datos y se lo pasamos al metodo correspondiente del controller
				Cliente newCliente = new Cliente();
				newCliente.setNombre(inputNombre.getText());
				newCliente.setApellido(inputApellido.getText());
				newCliente.setDireccion(inputDireccion.getText());
				newCliente.setDni(inputDni.getText());
				newCliente.setFecha(inputFecha.getText());
				
				MainController.insertCliente(newCliente);
			}
		});
	}
}
