package model.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MyConnection {

	//Variables con los datos de conexion
	String bd = "UD22Ejercicio1";
	String user = "remote";
	String password = "pass";
	String url = "jdbc:mysql://192.168.0.170:3306/" + bd + "?useTimezone=true&serverTimezone=UTC";
	
	Connection conn = null;
	
	//Metodo que conecta con la base de datos
	public MyConnection() {
		try{
	         Class.forName("com.mysql.cj.jdbc.Driver");
	         conn = DriverManager.getConnection(url,user,password);

	         if (conn!=null){
	            System.out.print("Conexión a base de datos " + bd + "_SUCCESS ");
	         }
	      }catch(SQLException e){
	         System.out.println(e);
	      }catch(ClassNotFoundException e){
	         System.out.println(e);
	      }catch(Exception e){
	         System.out.println(e);
	      }
	   }

	//Getter de la conexion
	public Connection getConn() {
		return conn;
	}
	
	//Desconector de la conexion
	public void desconectar(){
		conn = null;
	}
}
