package model.service;

import javax.swing.JOptionPane;

import model.dao.ClienteDao;
import model.dto.Cliente;

public class MainService {
	
	//Metodos de validacion que reciben un objeto cliente des del controller, le hace unas validaciones y si las pasa, envia el objeto recibido al Dao, que se encargara de la insercion
	public static void validateInsert(Cliente newCliente) {
		ClienteDao newClienteDao;
		
		if (newCliente.getNombre().length() > 1 && newCliente.getApellido().length() > 1) {
			newClienteDao = new ClienteDao();
			newClienteDao.insertCliente(newCliente);
		}else {
			JOptionPane.showMessageDialog(null,"El nombre y apellido no pueden estar vacios","Advertencia",JOptionPane.WARNING_MESSAGE);
			
		}
	}
	
	public static void validateUpdate(Cliente newCliente) {
		ClienteDao newClienteDao;
		
		if (newCliente.getNombre().length() > 1 && newCliente.getApellido().length() > 1) {
			newClienteDao = new ClienteDao();
			newClienteDao.updateCliente(newCliente);
		}else {
			JOptionPane.showMessageDialog(null,"El nombre y apellido no pueden estar vacios","Advertencia",JOptionPane.WARNING_MESSAGE);
			
		}
	}
	
	public static void validateRead(Cliente newCliente) {
		ClienteDao newClienteDao;
		
		if (newCliente.getId() >= 1) {
			newClienteDao = new ClienteDao();
			newClienteDao.readCliente(newCliente);
		}else {
			JOptionPane.showMessageDialog(null,"El id no puede ser menor que 1","Advertencia",JOptionPane.WARNING_MESSAGE);
			
		}
	}
	
	public static void validateDelete(Cliente newCliente) {
		ClienteDao newClienteDao;
		
		if (newCliente.getId() >= 1) {
			newClienteDao = new ClienteDao();
			newClienteDao.deleteCliente(newCliente);
		}else {
			JOptionPane.showMessageDialog(null,"El id no puede ser menor que 1","Advertencia",JOptionPane.WARNING_MESSAGE);
			
		}
	}
}
